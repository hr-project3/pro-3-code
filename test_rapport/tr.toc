\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Testplan}{1}% 
\contentsline {subsection}{\numberline {1.1}Scenario's}{2}% 
\contentsline {section}{\numberline {2}Testrapport}{3}% 
\contentsline {subsection}{\numberline {2.1}Uitkomsten}{3}% 
\contentsline {paragraph}{1. De gebruiker kan inloggen met een correcte combinatie van RFID en pincode}{3}% 
\contentsline {paragraph}{2. Het pinproces moet altijd afgebroken kunnen worden}{3}% 
\contentsline {paragraph}{3. Het pinproces moet altijd afgebroken kunnen worden}{3}% 
\contentsline {paragraph}{4. Geef de gebruiker geen geld wanneer er onvoldoende saldo is en meld dit aan de gebruiker.}{3}% 
\contentsline {paragraph}{5. Na 3 foute inlog pogingen word de pas geblokkeerd, en na een correcte inlog poging zal wordt deze counter gereset.}{3}% 
\contentsline {paragraph}{6. De gebruiker kan elke combinatie van biljetten kiezen van het gewenste bedrag.}{3}% 
\contentsline {paragraph}{7. De gebruiker kan zelf een bedrag kunnen intoetsen om op te kunnen nemen}{3}% 
\contentsline {paragraph}{8. Ongewenste input van hackers kan niet binnen komen via de client software.}{4}% 
