/* LIBRARIES */

#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>	//https://github.com/miguelbalboa/rfid

/* PIN VARIABLES */

const byte amount_rows = 4;
const byte amount_cols = 4;
const byte pins_row[amount_rows] = {9, 8, 7, 6};
const byte pins_col[amount_cols] = {5, 4, 3, 2};
const byte pin_rc5222_ss =  10;
const byte pin_rc5222_rst = 13;

/* CREATE MFRC522 */

MFRC522 mfrc522(pin_rc5222_ss, pin_rc5222_rst);

/* CREATE KEYPAD */

char keymap[amount_rows][amount_cols] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

Keypad pressed_keypad = Keypad(
	makeKeymap(keymap),
	pins_row,
	pins_col,
	amount_rows,
	amount_cols
);

/****************************************************************/

void setup(){
	Serial.begin(9600);
	init_rfid_reader();
}
  
void loop(){
	get_keypad_data();
	get_scanner_data();
}

/****************************************************************/

void init_rfid_reader(){
	SPI.begin();
	mfrc522.PCD_Init();
}

void get_keypad_data(){
	char pressed_key = pressed_keypad.getKey();
  
	if (pressed_key){
		Serial.print("KEY_");
		Serial.println(pressed_key);
	}
}

void get_scanner_data(){
	// Look for new cards, and check if serial is 
	if ( ! mfrc522.PICC_IsNewCardPresent()){return;}
	
	// Select one of the cards
	if ( ! mfrc522.PICC_ReadCardSerial()) {return;}
	
	Serial.print("CID_");
	for (byte i = 0; i < mfrc522.uid.size; i++){
		Serial.print(mfrc522.uid.uidByte[i], HEX);
		Serial.print(
			mfrc522.uid.uidByte[i] < 0x10 ? "" : " "
		);
	}
	Serial.println();
}
