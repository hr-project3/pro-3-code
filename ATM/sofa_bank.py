#!/bin/python

import MySQLdb
import serial
import pygame, sys
from pygame.locals import *

#----------------------------------------------------------------------------#

# setup mysql database
db = MySQLdb.connect(
    host="localhost", 
    user="root",
    passwd="root",  
    db="sofa_bank",
)

# cursor object to create queries
sql_cursor = db.cursor()

#----------------------------------------------------------------------------#

# currency variable to use globaly
currency = "€"

#----------------------------------------------------------------------------#

# global variabls
current_pygame_screen = "start_screen"
current_user = ''
current_rfid = ''
current_withdraw_amount = '0'

#----------------------------------------------------------------------------#

# Setup serial
# ttyUSB0 is configured for a linux machine
ser = serial.Serial('/dev/ttyUSB0',9600, timeout=0.001)

#----------------------------------------------------------------------------#

# Colors
white = (255,255,255)
red = (255,0,0)
black = (0,0,0)

# Window setup
pygame.init()
display_width = 1300
display_height = 900
tiny_text = pygame.font.Font('freesansbold.ttf', 18)
small_text = pygame.font.Font('/usr/share/fonts/noto/NotoSansMono-Regular.ttf', 25)
normal_text = pygame.font.Font('/usr/share/fonts/noto/NotoSansMono-Regular.ttf', 40)
large_text = pygame.font.Font('freesansbold.ttf', 60)
display = pygame.display.set_mode((display_width, display_height))

def text_objects(text, font, color):
    text_surface = font.render(text, True, color)
    return text_surface, text_surface.get_rect()

#----------------------------------------------------------------------------#

def is_card_blocked():
    sql_cursor.execute("select rfid from card where failed_logins>=3")
    print(current_rfid)
    for row in sql_cursor.fetchall():
        if row[0] == current_rfid:
            return True
    return False

def screen_background():
    display.fill(white)
    img = pygame.image.load('sofa_logo.jpg')
    display.blit(img,((display_width/2)-170 , display_height/10))

def can_withdraw_current_amount():
    query="select balance from account where user_id=" + str(current_user) + ";"
    sql_cursor.execute(query)
    balance = 0
    for row in sql_cursor.fetchall():
        balance = row[0]

    #print(current_withdraw_amount)
    print(balance)
    if int(current_withdraw_amount) <= int(balance):
        return True
    else:
        current_pygame_screen = "unsuccesfull_withdraw_screen"
        return False

# When chosing bills, an array keeps the chosen bill information.
# this function lets you chance the array
def bill_array_shifter(bill_array, bill_array_ammounts, index):
    print("bill_array_shifter ", bill_array_shifter)
    print("bill_array_ammounts", bill_array_ammounts)
    print("index              ", index)

    if index == 0:
        return bill_array_ammounts
    elif bill_array_ammounts[index-1] > 0:
        bill_array_ammounts[index] = bill_array_ammounts[index] + (bill_array[index-1]/bill_array[index])
        bill_array_ammounts[index-1] = bill_array_ammounts[index-1] - 1
    return bill_array_ammounts
    
#----------------------------------------------------------------------------#

def start_screen():
    global current_user
    global current_rfid
    global current_pygame_screen

    while True:
        screen_background()
        
        TextSurf2, TextRect2 = text_objects("Plaats up kaart op de scanner...", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/2+40))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        # SQL cursor query 
        sql_cursor.execute("SELECT rfid FROM user;")
        
        # Check RFID
        for row in sql_cursor.fetchall():
            if serial_identifier_array[0] != "":
                rfid = serial_identifier_array[1]
                if row[0] == rfid:

                    # Get current user id
                    query= "SELECT user_id FROM user where rfid='" + rfid + "';"
                    sql_cursor.execute(query)
                    for row in sql_cursor.fetchall():
                        current_user=row[0]

                    # Get current rfid
                    current_rfid = rfid

                    current_pygame_screen = "login_screen"
                    return
        
def login_screen():
    global current_pygame_screen
    input_pin_keys = ''
    input_pin_keys_hidden = ''

    # Go to blocked screen if card is blocked

    while True:
        screen_background()

        if is_card_blocked() == True:
            current_pygame_screen = "blocked_screen"
            return

        # Get serial data
        # format of data is: IDENTIFIER_VALUE
        # example: UID_00 00 00 00

        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        # Check keypresses
        if serial_identifier_array[0] == 'KEY':
            if serial_identifier_array[1] == '#':
                current_pygame_screen = 'start_screen'
                return
            elif serial_identifier_array[1] == 'A':
                # SQL to get the pincode from the card
                query = "SELECT pin FROM card WHERE rfid='" + current_rfid +  "';"
                sql_cursor.execute(query)
                
                for row in sql_cursor.fetchall():
                    if serial_identifier_array[0] != "":
                        user_pincode = row[0]
        
                        # Check if input pincode is correct
                        if len(input_pin_keys) == 4 and user_pincode == input_pin_keys:
                            current_pygame_screen = "menu_screen"
                            query = "update card set failed_logins=0 where rfid = '" + current_rfid + "';"
                            sql_cursor.execute(query)
                            db.commit()
                            return
                        elif len(input_pin_keys) == 4 and user_pincode != input_pin_keys:
                            query = "update card set failed_logins=failed_logins + 1 where rfid = '"
                            query += current_rfid + "';"
                            sql_cursor.execute(query)
                            db.commit()
                            input_pin_keys = ''
                            input_pin_keys_hidden = ''
            elif serial_identifier_array[1] =='B':
                input_pin_keys = ''
                input_pin_keys_hidden = ''
            else:
                if len(input_pin_keys) < 4:
                    input_pin_keys += serial_identifier_array[1]
                    input_pin_keys_hidden += "#"

        TextSurf2, TextRect2 = text_objects(input_pin_keys_hidden, large_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.7))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("A Conformeer pincode      ", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.7)+50)
        display.blit(TextSurf2, TextRect2)
        
        TextSurf2, TextRect2 = text_objects("B reset ingevoerde pincode", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.7)+90)
        display.blit(TextSurf2, TextRect2)

        textsurf2, textrect2 = text_objects("Vul uw 4 cijferige pincode in", small_text, black)
        textrect2.center = ((display_width/2), (display_height/2))
        display.blit(textsurf2, textrect2)

        TextSurf2, TextRect2 = text_objects("# Terug naar het startscherm", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()


def menu_screen():
    global current_pygame_screen

    while True:
        screen_background()

        textsurf2, textrect2 = text_objects("A Check saldo ", small_text, black)
        textrect2.center = ((display_width/2), ((display_height/2)+10))
        display.blit(textsurf2, textrect2)

        textsurf2, textrect2 = text_objects("B Geld opnemen", small_text, black)
        textrect2.center = ((display_width/2), ((display_height/2)+50))
        display.blit(textsurf2, textrect2)

        textsurf2, textrect2 = text_objects("C Snel €50    ", small_text, black)
        textrect2.center = ((display_width/2), ((display_height/2)+90))
        display.blit(textsurf2, textrect2)

        TextSurf2, TextRect2 = text_objects("* Breek sessie direct af", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            if serial_identifier_array[1] == "A":
                current_pygame_screen = 'saldo_screen'
                return
            if serial_identifier_array[1] == "B":
                current_pygame_screen = 'withdraw_amount_screen'
                return
            if serial_identifier_array[1] == "C":
                current_withdraw_amount = 50
                if can_withdraw_current_amount() == True:
                    query = "update account set balance=balance-" 
                    query += str(current_withdraw_amount) +" where user_id=" + str(current_user)
                    sql_cursor.execute(query)
                    db.commit()
                    current_pygame_screen = 'succesfull_withdraw_screen'
                    return
                else:
                    current_pygame_screen = 'unsuccesfull_withdraw_screen'
                    return

            if serial_identifier_array[1] == "*":
                current_pygame_screen = 'start_screen'
                return

def saldo_screen():
    global current_pygame_screen
    global current_user 

    # get balance of user
    query="select balance from account where user_id=" + str(current_user) + ";"
    sql_cursor.execute(query)
    balance = 0
    for row in sql_cursor.fetchall():
        balance = row[0]

    while True:
        screen_background()

        message =  currency + str(balance) 
        textsurf2, textrect2 = text_objects(str(message), large_text, black)
        textrect2.center = ((display_width/2), ((display_height/2)+10))
        display.blit(textsurf2, textrect2)

        textsurf2, textrect2 = text_objects("Druk op een toets om terug te gaan naar het menu", tiny_text, black)
        textrect2.center = ((display_width/2), ((display_height/2)+80))
        display.blit(textsurf2, textrect2)

        TextSurf2, TextRect2 = text_objects("* Breek sessie direct af", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2))
        display.blit(TextSurf2, TextRect2)


        pygame.display.update()

        # Check keypresses to go to next screen
        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            if serial_identifier_array[1] == "*":
                current_pygame_screen = 'start_screen'
                return
            if serial_identifier_array[1] != "":
                current_pygame_screen = 'menu_screen'
                return

def withdraw_amount_screen():
    global current_pygame_screen
    global current_withdraw_amount
    global currency
    current_withdraw_amount = '0'

    while True:
        screen_background()

        TextSurf2, TextRect2 = text_objects("# Terug naar het sneltoets menu", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("* Breek sessie direct af                ", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2)+20)
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("Toets het bedrag in wat u wilt opnemen", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/2))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("A Ga door en kies uw biljetten", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.5))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("B Reset het bedrag            ", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.4))
        display.blit(TextSurf2, TextRect2)

        message = currency + str(int(current_withdraw_amount))
        TextSurf2, TextRect2 = text_objects((message), large_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.7))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            if serial_identifier_array[1] == "A":
                if int(current_withdraw_amount) == 0:
                    print("INPUT ERROR: Can't go to bill choser screen if current_withdraw_amount is zero")
                elif can_withdraw_current_amount() == True:
                    current_pygame_screen = 'bill_choser_screen'
                    return
                else:
                    current_pygame_screen = 'unsuccesfull_withdraw_screen'
                    return
            elif serial_identifier_array[1] == '#':
                current_pygame_screen = 'menu_screen'
                return
            elif serial_identifier_array[1].isdigit():
                if current_withdraw_amount == '0':
                    current_withdraw_amount = ''
                current_withdraw_amount += str(serial_identifier_array[1])
            elif serial_identifier_array[1] == '*':
                current_pygame_screen = 'start_screen'
                return
            elif serial_identifier_array[1] == 'B':
                current_withdraw_amount = '0'


def bill_choser_screen():
    global current_pygame_screen
    global current_withdraw_amount
    global currency

    print("current_withdraw_amount: ", current_withdraw_amount)

    bill_algoritm_setup = False

    while True:
        screen_background() 

        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        TextSurf2, TextRect2 = text_objects("Toets de nummers in om het aantal te veranderen", small_text, black)
        TextRect2.center = (((display_width/2)), (display_height/2))
        display.blit(TextSurf2, TextRect2)

        # Algoritm for incrementing bills

        if bill_algoritm_setup == False:
            x = int(current_withdraw_amount)
            bill_algoritm_setup = True
            bill_array = [100, 50, 10, 1]
            bill_array_length = len(bill_array)
            bill_array_ammounts = [0] * bill_array_length
            bill_index = 0
    
            for current_bill in bill_array:
                while x >= current_bill:
                    bill_array_ammounts[bill_index] += 1
                    x -= current_bill
                bill_index += 1

        # OPTIONS
        sepparator = "  "

        times = str(bill_array_ammounts[0])
        bill_string = "  " + sepparator + currency + "100" + sepparator + "x" + times

        TextSurf2, TextRect2 = text_objects(bill_string, small_text, black)
        TextRect2.center = (((display_width/2)), (display_height/1.8))
        display.blit(TextSurf2, TextRect2)

        bill_string = "1." + sepparator + currency + " " + "50" + sepparator + "x" + str(round(bill_array_ammounts[1]))

        TextSurf2, TextRect2 = text_objects(bill_string, small_text, black)
        TextRect2.center = (((display_width/2)), (display_height/1.7))
        display.blit(TextSurf2, TextRect2)

        bill_string = "2." + sepparator + currency + " " + "10" + sepparator + "x" + str(round(bill_array_ammounts[2]))

        TextSurf2, TextRect2 = text_objects(bill_string, small_text, black)
        TextRect2.center = (((display_width/2)), (display_height/1.6))
        display.blit(TextSurf2, TextRect2)

        bill_string = "3." + sepparator + currency + "  " + "1" + sepparator + "x" + str(bill_array_ammounts[3])

        TextSurf2, TextRect2 = text_objects(bill_string, small_text, black)
        TextRect2.center = (((display_width/2)), (display_height/1.5))
        display.blit(TextSurf2, TextRect2)

        # TEXT
        message = "Uw bedrag: " + currency + current_withdraw_amount
        TextSurf2, TextRect2 = text_objects(message, small_text, black)
        TextRect2.center = ((display_width/2), (display_height/2.2))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("# Terug naar het sneltoets menu", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("* Breek sessie direct af                ", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2)+20)
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("A Neem het geld op ", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.3)-40)
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects("B Reset uw voorkeur", small_text, black)
        TextRect2.center = ((display_width/2), (display_height/1.3))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        if serial_identifier_array[0] == "KEY":
            if serial_identifier_array[1] == '#':
                current_pygame_screen = 'menu_screen'
                return
            elif serial_identifier_array[1] == '*':
                current_pygame_screen = 'start_screen'
                return
            elif serial_identifier_array[1] == 'A':
                query = "update account set balance=balance-" 
                query += str(current_withdraw_amount) +" where user_id=" + str(current_user)
                sql_cursor.execute(query)
                db.commit()
                current_pygame_screen = 'succesfull_withdraw_screen'
                return
            elif serial_identifier_array[1] == 'B':
                bill_algoritm_setup = False
            elif serial_identifier_array[1] == '1':
                bill_array_ammounts = bill_array_shifter(bill_array, bill_array_ammounts, 1)
            elif serial_identifier_array[1] == '2':
                bill_array_ammounts = bill_array_shifter(bill_array, bill_array_ammounts, 2)
            elif serial_identifier_array[1] == '3':
                bill_array_ammounts = bill_array_shifter(bill_array, bill_array_ammounts, 3)

def blocked_screen():
    global current_pygame_screen

    while True:
        
        TextSurf2, TextRect2 = text_objects(
            "Deze kaart in geblokkeerd. Druk op een toets of terug te keren naar het startmenu", 
            small_text, 
            black
        )

        screen_background()

        TextRect2.center = ((display_width/2), (display_height/2+40))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        # Check keypresses to go to next screen
        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            print("Back to start screen")
            current_pygame_screen = "start_screen"
            return

def unsuccesfull_withdraw_screen():
    global current_pygame_screen

    while True:

        screen_background()

        TextSurf2, TextRect2 = text_objects(
            "U heeft niet voldoende geld om dit bedrag af op te kunnen nemen",
            small_text, black)

        TextRect2.center = ((display_width/2), (display_height/2+40))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects(
            "Druk op een toets om terug keren naar het sneltoetmenu.",
            small_text, black)

        TextSurf2, TextRect2 = text_objects("Breek sessie direct af *", tiny_text, red)
        TextRect2.center = ((display_width/2), (display_height/1.2)+20)
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        # Check keypresses to go to next screen
        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            current_pygame_screen = 'menu_screen'
            return

def succesfull_withdraw_screen():
    global current_pygame_screen
    global current_user
    global current_rfid

    while True:
        screen_background()

        TextSurf2, TextRect2 = text_objects(
            "Het geld is succesvol van uw rekening afgeschreven.", 
            small_text, black)

        TextRect2.center = ((display_width/2), (display_height/2+40))
        display.blit(TextSurf2, TextRect2)

        TextSurf2, TextRect2 = text_objects(
            "Druk op een toets om sessie af te breken en om terug keren naar het start menu.",
            small_text, black)

        TextRect2.center = ((display_width/2), (display_height/2+90))
        display.blit(TextSurf2, TextRect2)

        pygame.display.update()

        # Check keypresses to go to next screen
        raw = ser.readline()
        serial_data = raw.strip().decode("utf-8")
        serial_identifier_array = serial_data.split("_")

        if serial_identifier_array[0] == "KEY":
            current_user = ''
            current_rfid = ''
            current_pygame_screen = 'start_screen'
            return

#----------------------------------------------------------------------------#

def main():
    while True:
        if current_pygame_screen == "start_screen":
            start_screen()
        if current_pygame_screen == "login_screen":
            login_screen()
        if current_pygame_screen == "menu_screen":
            menu_screen()
        if current_pygame_screen == 'withdraw_amount_screen':
            withdraw_amount_screen()
        if current_pygame_screen == 'saldo_screen':
            saldo_screen()
        if current_pygame_screen == 'blocked_screen':
            blocked_screen()
        if current_pygame_screen == 'succesfull_withdraw_screen':
            succesfull_withdraw_screen()
        if current_pygame_screen == 'unsuccesfull_withdraw_screen':
            unsuccesfull_withdraw_screen()
        if current_pygame_screen == 'bill_choser_screen':
            bill_choser_screen()
main()

#----------------------------------------------------------------------------#
