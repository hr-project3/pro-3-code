create database sofa_bank;
use sofa_bank;

create table user(
	user_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	first_name VARCHAR(30) NOT NULL,
	last_name VARCHAR(30) NOT NULL,
	date_of_birth DATE NOT NULL,
	email VARCHAR(60) NOT NULL,
	postcode VARCHAR(10) NOT NULL,
	street VARCHAR(60) NOT NULL,
	street_number VARCHAR(15) NOT NULL,
	phone_number VARCHAR(30) NOT NULL,
	sex ENUM('M','F') NOT NULL,
	rfid VARCHAR(60) NOT NULL
);

create table card(
	rfid VARCHAR(60) NOT NULL,
	pin VARCHAR(4) NOT NULL ,
	failed_logins INT NOT NULL,
	PRIMARY KEY (rfid)
) ENGINE=InnoDB;

create table account(
	acount_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	balance INT NOT NULL,
	user_id INT NOT NULL
);

create table log(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	message VARCHAR(60) NOT NULL,
	date_of_message TIMESTAMP NOT NULL

);

insert into user value(
	NULL,
	'Henk', 'Bakker',
	'2000-2-22',
	'henk@henk.nl',
	'3423EG',
	'Straatlaan', '23',
	'+3162453784',
	'M',
	'2D 4D DE 2B');

insert into card value(
	'2D 4D DE 2B',
	'1234',
	0
);

insert into account value(
	NULL,
	1000000,
	1
);
